<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //Each Employee has a Client
    public function client(){
        return $this->belongsTo('App\Client');
    }

    //An Employee has many EmploymentEvents
    public function employmentevents(){
        return $this->hasMany('App\EmploymentEvent');
    }
}