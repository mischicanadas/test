<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentEvent extends Model
{
    //Each EmploymentEvent has an Employee
    public function employee(){
        return $this->belongsTo('App\Employee');
    }
}
