<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //A Client has many employees
    public function employees(){
        return $this->hasMany('App\Employee');
    }
}
