@extends('layouts.app')

@section('content')
    <h1>Clients</h1>
    @if(count($clients)>1)
        @foreach($clients as $client)
            <div class="well">
                <h5>{{$client->name}}
            </div>
        @endforeach
    @else
        <p>No clients to show</p>
    @endif
@endsection