<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="{{ asset('js/app.js') }}"></script>

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>

        <title>{{ config('app.name','Events') }}</title>
    </head>
    <body>
        
        <main class="container-fluid wrap">
            <div id="div_contenido" class="inner-wrap">
                @yield('content')
            </div>
        </main>
        <script src="{{asset('js/helpers.js')}}"></script>
        <script src="{{asset('js/bootstrap.js')}}"></script>
        <script src="{{asset('js/custom_dropdown.js')}}"></script>
        <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    </body>
</html>