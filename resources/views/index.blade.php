@extends('layouts.app')

@section('content')
    
<div class="main main-raised">

    <div class="container-fluid">

        <div style="padding-left: 30px; padding-right: 30px;">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <h1>Records</h1>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 pull-right">
                    <h1 class="text-center title" style="margin: 5px -50px 0 0; font-size: 6em;">{{App\Client::count() + App\Employee::count() + App\EmploymentEvent::count()}}</h1>
                </div>
            </div>

            <hr>
            <span>Records by table</span>
            
            <div class="row">
                <div class="col-xm-12 col-sm-12 col-md-12">
                    <div class="well">
                        <h5>Clients: <strong>{{App\Client::count()}}</strong></h5>
                        <h5>Employees: <strong>{{App\Employee::count()}}</strong></h5>
                        <h5>Employment Events: <strong>{{App\EmploymentEvent::count()}}</strong></h5>
                    </div>
                </div>
                
            </div>

            <br><br>
            

        </div> <!-- Inside Container -->
    </div> <!-- Container -->
</div> <!-- Main -->
@endsection